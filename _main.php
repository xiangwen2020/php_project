<?php
require_once '_setup.php';

$app->get('/internalerror', function ($request, $response, $args) {
    return $this->view->render($response, 'error_internal.html.twig');
});

// TODO: Define app routes
// Define app routes
$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response,'index.html.twig' );
});


// NOTE: $_SESSION or $_FILES work the same way as they did before
