<?php
// require_once '_setup.php';

# this file is for /admin/* URL handlers

// Attach middleware that verifies only Admin can access /admin... URLs

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use Respect\Validation\Validator as Validator;

//File upload directory
$container['upload_directory'] = __DIR__ . '/images';
// Function to check string starting 
// with given substring 
function startsWith($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 

$app->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
    $url = $request->getUri()->getPath();
    if (startsWith($url, "/admin")) {
        if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not logged in AS ADMIN
            $response = $response->withStatus(403);
            return $this->view->render($response, 'error_access_denied.html.twig');
        }
    }
    return $next($request, $response);
});
//index
$app->get('/admin', function ($request, $response, $args) {
    return $this->view->render($response, 'admin/master.html.twig');
});

//ADMIN BOUQUET MANAGEMENT PART
use Slim\Http\UploadedFile;

// Create and configure Slim app
// $config = ['settings' =>[
//     'addContentLengthHeader' => false,
//     'displayErrorDetails' =>true
// ]];
// // Fetch DI Container
// $container = $app->getContainer();
// // File upload directory
// $container['upload_directory'] = __DIR__ . '/images';

// // Register Twig View helper
// $container['view'] = function($c){
//     $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates',[
//             'cache' => dirname(__FILE__) . '/cache',
//             'debug' => true,    //enable for debug mode
//     ]);
    // Instantiate and add Slim specific extension
//     $router = $c->get('router');
//     $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
//     $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
//     return $view;
// };
//admin/bouquets
$app->get('/admin/bouquets/list', function ($request, $response, $args) {
    
    // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
    //     $response = $response->withStatus(403);
    //     return $this->view->render($response, 'error_access_denied.html.twig');
    // }
    $bouquetList = DB::query("SELECT * FROM bouquets");
        
    return $this->view->render($response, 'admin/bouquets_list.html.twig', ['bouquets' => $bouquetList]);
                                                           
    //print_r($articleList);
    //return $response->write("");
});


//admin/users/op:{edit|add}
// STATE 1: first display
$app->get('/admin/bouquets/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($args['op'] == 'add' && !empty($args['id'])) || ($args['op'] == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($args['op'] == 'edit') {
        $bouquet = DB::queryFirstRow("SELECT * FROM bouquets WHERE id=%d", $args['id']);
        if (!$bouquet) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $bouquet = [];
    }
    return $this->view->render($response, 'admin/bouquets_addedit.html.twig', ['v' => $bouquet, 'op' => $args['op']]);
});

// STATE 2&3: receiving submission
$app->post('/admin/bouquets/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {

    $op = $args['op'];
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($op == 'add' && !empty($args['id'])) || ($op == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    $name = $request->getParam('name');
    $description = $request->getParam('description');
    $description = strip_tags($description, "<p><ul><li><em><strong><i><b><ol><h3><h4><h5><span>");
    $price = $request->getParam('price');
    $isShow = $request->getParam('isshow') ?? '0';
    //
    $errorList = array();

    $result = verifyUserName($name);
    if ($result != TRUE) { $errorList[] = $result; }

    if (!Validator::stringType()->length(2, 1000)->validate($description)) {
        // if (strlen($description) < 2 || strlen($description) > 1000) {
            array_push($errorList, "Description must be 2-1000 characters long");
            // keep the description even if invalid
        }
    
    if ($price < 0){
        $price = 0;
    }
    //verify image
    $uploadedImagePath = null;
    // $uploadedImage = $request->getUploadedFiles()['photo'];
    // if($uploadedImage->getError()!= UPLOAD_ERR_NO_FILE){//if uploaded
    //     print_r($uploadedImage->getError());
    //     $result = verifyUploadedPhoto($uploadedImagePath, $uploadedImage);
    //     if($result !== TRUE){
    //         $errorList[] = $result;
    //     }
    // }

    if ($errorList) {
        return $this->view->render($response, 'admin/bouquets_addedit.html.twig',
                [ 'errorList' => $errorList, 'v' => ['name' => $name, 'description' => $description, 'price' =>$price, 'isshow' =>$isShow ]  ]);
    } else {

        if ($op == 'add') {
            DB::insert('bouquets', ['name' => $name, 'description' => $description, 'price' =>$price, 'isshow' =>$isShow ]);
            $id = DB::insertId();
            $uploadedImage = $request->getUploadedFiles()['photo'];
            if($uploadedImage->getError()!= UPLOAD_ERR_NO_FILE){//if uploaded
                print_r($uploadedImage->getError());
                $result = verifyUploadedPhoto($uploadedImagePath, $uploadedImage, $id);
                if($result !== TRUE){
                    $errorList[] = $result;
                }
            }
            if($uploadedImagePath !=null){
                $directory = $this->get('upload_directory');
                $uploadedImagePath = moveUploadedFile($directory, $uploadedImage, $id);
            }
            DB::update('bouquets',['imagefilepath' =>$uploadedImagePath],"id=%d", $id);
            return $this->view->render($response, 'admin/bouquets_addedit_success.html.twig', ['op' => $op ]);
        } else {
                //verify image
    
                $uploadedImage = $request->getUploadedFiles()['photo'];
                if($uploadedImage->getError()!= UPLOAD_ERR_NO_FILE){//if uploaded
                    print_r($uploadedImage->getError());
                    $result = verifyUploadedPhoto($uploadedImagePath, $uploadedImage, $args['id']);
                    if($result !== TRUE){
                        $errorList[] = $result;
                    }
                }
            $data = ['name' => $name, 'description' => $description, 'price' =>$price, 'imagefilepath' =>$uploadedImagePath, 'isshow' =>$isShow ];

            if($uploadedImagePath !=null){
                $directory = $this->get('upload_directory');
                $uploadedImagePath = moveUploadedFile($directory, $uploadedImage, $args['id']);
            }
            DB::update('bouquets', $data, "id=%d", $args['id']);
            return $this->view->render($response, 'admin/bouquets_addedit_success.html.twig', ['op' => $op ]);
        }
    }
});


// STATE 1: first display
$app->get('/admin/bouquets/delete/{id:[0-9]+}', function ($request, $response, $args) {

    $bouquet = DB::queryFirstRow("SELECT * FROM bouquets WHERE id=%d", $args['id']);
    if (!$bouquet) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/bouquets_delete.html.twig', ['v' => $bouquet] );
});

// STATE 2&3: receiving submission
$app->post('/admin/bouquets/delete/{id:[0-9]+}', function ($request, $response, $args) {

    DB::delete('bouquets', "id=%d", $args['id']);
    return $this->view->render($response, 'admin/bouquets_delete_success.html.twig' );
});



// returns TRUE on success
// returns a string with error message on failure
function verifyUploadedPhoto(&$photoFilePath, $photo, $id) {
    if (isset($_FILES['photo']) && $_FILES['photo']['error'] != 4) { // file uploaded
        // print_r($_FILES);
        $photo = $_FILES['photo'];
        if ($photo['error'] != 0) {
            return "Error uploading photo " . $photo['error'];
        } 
        if ($photo['size'] > 2*1024*1024) { // 1MB
            return "File too big. 2MB max is allowed.";
        }
        $info = getimagesize($photo['tmp_name']);
        if (!$info) {
            return "File is not an image";
        }

        if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
            return "Width and height must be within 200-1000 pixels range";
        }
        
        switch ($info['mime']) {
            case 'image/jpeg': $ext = "jpg"; break;
            case 'image/gif': $ext = "gif"; break;
            case 'image/png': $ext = "png"; break;
            default:
                return "Only JPG, GIF and PNG file types are allowed";
            }
            $baseName = "bouquet_" . $id;
            $photoFilePath =  $baseName . "." . $ext;
            return TRUE;
        // $photoFilePath = "uploads/" .  $bouquetId . "." . $ext;
    }
}

function moveUploadedFile($directory, UploadedFile $uploadedFile, $id)
{
    //$uploadedFile->file;
    // $uploadedFile->
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    // $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $basename = 'bouquet_' . $id;
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

//ADMIN ORDER MANAGEMENT PART
// $app->get('/admin/orders/list', function ($request, $response, $args) {
//     // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
//     //     $response = $response->withStatus(403);
//     //     return $this->view->render($response, 'error_access_denied.html.twig');
//     // }
//     $orderList = DB::query("SELECT o.id AS id, orderNumber, orderTS, name, totalprice, ispaid, address FROM orders AS o JOIN users AS u ON o.userid = u.id");
    
//     return $this->view->render($response, 'admin/order_list.html.twig', ['orders' => $orderList]);
                                                           
//     //print_r($articleList);
//     //return $response->write("");
// });

$app->get('/admin/orders/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {

    $sql = "SELECT o.id AS id, orderNumber, orderTS, name, totalprice, ispaid, address FROM orders AS o JOIN users AS u ON o.userid = u.id";
    $perPage = 5;
    $currPage = $args['pageNo'] ?? 1;
    $pagedQuery = queryPagination($sql, $perPage, $currPage);
    $orderList = $pagedQuery['query'];
    $maxPage = $pagedQuery['maxPage'];
    return $this->view->render($response, 'admin/order_list_paged.html.twig', [
        'orders' => $orderList,
        'maxPage' => $maxPage,
        'currPage' => $currPage]);

});

// PAGINATION WITH AJAX
// $app->get('/admin/orders/listpaged[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
//     $ordersPerPage = 5;
//     $pageNo = $args['pageNo'] ?? 1;
//     $ordersCount = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM orders");
//     $maxPages = ceil($ordersCount / $ordersPerPage);
//     return $this->view->render($response, 'admin/order_list_paged.html.twig', [
//             'maxPages' => $maxPages,
//             'pageNo' => $pageNo,
//         ]);
// });
// $app->get('/admin/orders/listSinglepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
//     global $articlesPerPage;
//     $pageNo = $args['pageNo'] ?? 1;
//     $articleList = DB::query("SELECT a.id, a.authorId, a.creationTS, a.title, a.body, u.name "
//         . "FROM articles as a, users as u WHERE a.authorId = u.id ORDER BY a.id DESC LIMIT %d OFFSET %d",
//              $articlesPerPage, ($pageNo - 1) * $articlesPerPage);
//     foreach ($articleList as &$article) {
//         // format posted date
//         $datetime = strtotime($article['creationTS']);
//         $postedDate = date('M d, Y \a\t H:i:s', $datetime );
//         $article['postedDate'] = $postedDate;
//         // only show the beginning of body if it's long, also remove html tags
//         $fullBodyNoTags = strip_tags($article['body']);
//         $bodyPreview = substr(strip_tags($fullBodyNoTags), 0, 100); // FIXME
//         $bodyPreview .= (strlen($fullBodyNoTags) > strlen($bodyPreview)) ? "..." : "";
//         $article['body'] = $bodyPreview;
//     }
//     return $this->view->render($response, 'ajaxsinglepage.html.twig', [
//             'list' => $articleList
//         ]);
// });

// STATE 1: first display
$app->get('/admin/orders/delete/{id:[0-9]+}', function ($request, $response, $args) {

    $order = DB::queryFirstRow("SELECT * FROM orders WHERE id=%d", $args['id']);
    $customerDetail = DB::queryFirstRow("SELECT u.id, u.name, u.address, u.phone,o.orderTS FROM orders AS o JOIN users AS u ON o.userid = u.id WHERE o.id=%d", $args['id']);
    $orderDetail = DB::query("SELECT o.id AS orderId, o.orderNumber, o.userid, o.orderTS, o.totalprice, o.ispaid, d.bouquetid, d.quantity, b.id AS bouquetId, b.name AS bouquetName, b.price AS bouquetPrice FROM orders AS o JOIN orderdetails AS d ON o.orderNumber = d.orderNumber JOIN bouquets AS b ON d.bouquetid = b.id WHERE o.id = %d", $args['id']);
    if (!$order) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/orders_delete.html.twig', ['o' => $order, 'c' => $customerDetail, 'd' => $orderDetail] );
});

// STATE 2&3: receiving submission
$app->post('/admin/orders/delete/{id:[0-9]+}', function ($request, $response, $args) {

    DB::delete('orders', "id=%d", $args['id']);
    return $this->view->render($response, 'admin/orders_delete_success.html.twig' );
});

$app->get('/admin/orders/orderdetail/{id:[0-9]+}', function ($request, $response, $args) {
    // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
    //     $response = $response->withStatus(403);
    //     return $this->view->render($response, 'error_access_denied.html.twig');
    // }
    $customerDetail = DB::queryFirstRow("SELECT u.id, u.name, u.address, u.phone,o.orderTS FROM orders AS o JOIN users AS u ON o.userid = u.id WHERE o.id=%d", $args['id']);
    $orderDetail = DB::query("SELECT o.id AS orderId, o.orderNumber, o.userid, o.orderTS, o.totalprice, o.ispaid, d.bouquetid, d.quantity, b.id AS bouquetId, b.name AS bouquetName, b.price AS bouquetPrice FROM orders AS o JOIN orderdetails AS d ON o.orderNumber = d.orderNumber JOIN bouquets AS b ON d.bouquetid = b.id WHERE o.id = %d", $args['id']);
    
    return $this->view->render($response, 'admin/order_detail.html.twig', ['orderdetails' => $orderDetail, 'customerdetail' => $customerDetail]);
                                                           
    //print_r($articleList);
    //return $response->write("");
});

// ADMIN USER MANAGEMENT PART
//admin/users/list
// $app->get('/admin/users/list', function ($request, $response, $args) {
//     // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
//     //     $response = $response->withStatus(403);
//     //     return $this->view->render($response, 'error_access_denied.html.twig');
//     // }
//     $usersList = DB::query("SELECT * FROM users");
//     return $this->view->render($response, 'admin/users_list.html.twig', ['usersList' => $usersList]);
// });

// PAGINATION WITH AJAX
$app->get('/admin/users/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $sql = "SELECT * FROM users";
    $perPage = 5;
    $currPage = $args['pageNo'] ?? 1;
    $pagedQuery = queryPagination($sql, $perPage, $currPage);
    $usersList = $pagedQuery['query'];
    $maxPage = $pagedQuery['maxPage'];
    return $this->view->render($response, 'admin/users_listpaginated.html.twig', [
        'usersList' => $usersList,
        'maxPage' => $maxPage,
        'currPage' => $currPage
        ]);
});
//admin/users/op:{edit|add}
// STATE 1: first display
$app->get('/admin/users/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
    //     $response = $response->withStatus(403);
    //     return $this->view->render($response, 'error_access_denied.html.twig');
    // }
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($args['op'] == 'add' && !empty($args['id'])) || ($args['op'] == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($args['op'] == 'edit') {
        $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%d", $args['id']);
        if (!$user) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $user = [];
    }
    return $this->view->render($response, 'admin/users_addedit.html.twig', ['v' => $user, 'op' => $args['op']]);
});

// STATE 2&3: receiving submission
$app->post('/admin/users/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
    //     $response = $response->withStatus(403);
    //     return $this->view->render($response, 'error_access_denied.html.twig');
    // }
    $op = $args['op'];
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($op == 'add' && !empty($args['id'])) || ($op == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    $name = $request->getParam('name');
    $isAdmin = $request->getParam('isAdmin') ?? '0';
    $email = $request->getParam('email');
    $phone = $request->getParam('phone');
    $address = $request->getParam('address');
    $postcode = $request->getParam('postcode');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');
    //
    $errorList = array();

    $result = verifyUserName($name);
    if ($result != TRUE) { $errorList[] = $result; }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
        $email = "";
    } else {
        // is email already in use BY ANOTHER ACCOUNT???
        if ($op == 'edit') {
            $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s AND id != %d", $email, $args['id'] );
        } else { // add has no id yet
            $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        }
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }
    // verify password always on add, and on edit/update only if it was given
    if ($op == 'add' || $pass1 != '') {
        $result = verifyPasswordQuailty($pass1, $pass2);
        if ($result != TRUE) { $errorList[] = $result; }
    }
    //
    if ($errorList) {
        return $this->view->render($response, 'admin/users_addedit.html.twig',
                [ 'errorList' => $errorList, 'v' => ['name' => $name, 'email' => $email ]  ]);
    } else {
        if ($op == 'add') {
            DB::insert('users', ['name' => $name, 'email' => $email, 'phone' =>$phone, 'address' => $address, 'postcode'=>$postcode, 'password' => $pass1, 'isAdmin' => $isAdmin]);
            return $this->view->render($response, 'admin/users_addedit_success.html.twig', ['op' => $op ]);
        } else {
            $data = ['name' => $name, 'email' => $email, 'isAdmin' => $isAdmin];
            if ($pass1 != '') { // only update the password if it was provided
                $data['password'] = $pass1;
            }
            DB::update('users', $data, "id=%d", $args['id']);
            return $this->view->render($response, 'admin/users_addedit_success.html.twig', ['op' => $op ]);
        }
    }
});


// STATE 1: first display
$app->get('/admin/users/delete/{id:[0-9]+}', function ($request, $response, $args) {
    // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
    //     $response = $response->withStatus(403);
    //     return $this->view->render($response, 'error_access_denied.html.twig');
    // }
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%d", $args['id']);
    if (!$user) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/users_delete.html.twig', ['v' => $user] );
});

// STATE 1: first display
$app->post('/admin/users/delete/{id:[0-9]+}', function ($request, $response, $args) {
    // if (!isset($_SESSION['user']) || !$_SESSION['user']['isadmin']) { // refuse if user not admin
    //     $response = $response->withStatus(403);
    //     return $this->view->render($response, 'error_access_denied.html.twig');
    // }
    DB::delete('users', "id=%d", $args['id']);
    return $this->view->render($response, 'admin/users_delete_success.html.twig' );
});

function verifyUserName($name) {
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $name) != 1) { // no match
        return "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.";
    }
    return TRUE;
}

function verifyPasswordQuailty($pass1, $pass2) {
    if ($pass1 != $pass2) {
        return "Passwords do not match";
    } else {
        if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            return "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }
    return TRUE;
}

function queryPagination($sql, $perPage, $currPage = 1){
    if($currPage < 1){
        $currPage = 1;
    }
    $sqlResult = DB::queryFirstColumn($sql);
    $count = count($sqlResult);
    $maxPage = ceil($count / $perPage);
    if($currPage > $maxPage){
        $currPage = $maxPage;
    }
    $newSql = $sql . " LIMIT %d OFFSET %d";
    $queryResult = DB::query($newSql, $perPage, ($currPage - 1) * $perPage);
    return ['query' => $queryResult, 'maxPage'=>$maxPage];
}


// Run app
$app->run();