-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2020 at 07:32 AM
-- Server version: 10.3.24-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp4976_rainbowbouquet`
--

-- --------------------------------------------------------

--
-- Table structure for table `bouquets`
--

CREATE TABLE `bouquets` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `imagefilepath` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `isshow` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bouquets`
--

INSERT INTO `bouquets` (`id`, `name`, `price`, `imagefilepath`, `description`, `isshow`) VALUES
(3, 'white rose', 45.99, 'd943eb5f6de4ee62.png', 'This is white rose', 0),
(4, 'red rose', 39.99, '6d23128cac416d6e.png', 'This is red rose', 1),
(5, 'Birthday Gift', 39.99, '6985af9c51a7f866.jpg', 'This is the best gift', 1),
(6, 'New Baby Gift ', 39.99, '9910c4e4b9b90f4b.png', 'This is the best gift for new baby', 1),
(7, 'New Baby Gift ', 35.99, 'bouquet_7.png', 'This is the best gift for new baby(girl)', 1),
(8, 'Sun Flower', 39.99, 'bouquet_8.png', 'This is sun flower', 1),
(9, 'Pink Rose', 45.99, 'bouquet_9.png', 'This is pink rose', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `ID` int(11) NOT NULL,
  `sessionID` varchar(50) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `createdTS` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`ID`, `sessionID`, `productID`, `quantity`, `createdTS`) VALUES
(6, '430d47ce635ba03ece2c9e924a452ba7', 4, 1, '2020-08-12 20:55:11'),
(8, '53ad7ec704971671814d0ac9b2ea633b', 4, 1, '2020-08-14 03:23:11'),
(10, '9506712d3f643b069b57e3f0a65bf20f', 6, 1, '2020-08-16 14:04:37'),
(11, '9506712d3f643b069b57e3f0a65bf20f', 7, 1, '2020-08-16 14:04:48'),
(14, '73ad0ebe0438b7a7f5048cd76146a541', 5, 1, '2020-08-17 02:24:03');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL,
  `orderNumber` varchar(30) NOT NULL,
  `bouquetid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`id`, `orderNumber`, `bouquetid`, `quantity`) VALUES
(1, '2020081103553', 3, 2),
(2, '2020081103553', 4, 2),
(3, '2020081104004', 4, 1),
(4, '2020081104004', 5, 1),
(5, '2020081104025', 5, 1),
(6, '2020081104136', 4, 1),
(7, '2020081104027', 3, 1),
(8, '2020081104027', 4, 1),
(9, '2020081104027', 5, 1),
(10, '2020081204048', 4, 6),
(11, '2020081204048', 3, 4),
(12, '2020081204048', 5, 6),
(13, '2020081204048', 6, 2),
(14, '2020081204149', 4, 1),
(15, '2020081204149', 5, 2),
(16, '20200812041411', 4, 1),
(17, '20200812041411', 5, 2),
(18, '20200812205212', 3, 1),
(19, '20200814032113', 3, 1),
(20, '20200816012514', 5, 1),
(21, '20200817015515', 5, 1),
(22, '20200817020716', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `orderNumber` varchar(30) NOT NULL,
  `userid` int(11) NOT NULL,
  `orderTS` timestamp NOT NULL DEFAULT current_timestamp(),
  `totalprice` decimal(10,2) NOT NULL,
  `ispaid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderNumber`, `userid`, `orderTS`, `totalprice`, `ispaid`) VALUES
(1, '2020081103531', 1, '2020-08-11 01:53:43', 85.98, 0),
(2, '2020081103542', 1, '2020-08-11 01:54:30', 85.98, 0),
(3, '2020081103553', 1, '2020-08-11 01:55:56', 85.98, 0),
(4, '2020081104004', 1, '2020-08-11 02:00:39', 79.98, 0),
(5, '2020081104025', 1, '2020-08-11 02:02:06', 39.99, 0),
(6, '2020081104136', 1, '2020-08-11 02:13:20', 39.99, 0),
(7, '2020081104027', 1, '2020-08-11 04:02:27', 125.97, 0),
(8, '2020081204048', 2, '2020-08-12 04:04:28', 165.96, 0),
(9, '2020081204149', 2, '2020-08-12 04:14:03', 79.98, 0),
(10, '20200812041410', 2, '2020-08-12 04:14:06', 79.98, 0),
(11, '20200812041411', 2, '2020-08-12 04:14:59', 79.98, 0),
(12, '20200812205212', 2, '2020-08-12 20:52:58', 45.99, 0),
(13, '20200814032113', 1, '2020-08-14 03:21:29', 45.99, 0),
(14, '20200816012514', 1, '2020-08-16 01:25:38', 39.99, 0),
(15, '20200817015515', 2, '2020-08-17 01:55:09', 39.99, 0),
(16, '20200817020716', 2, '2020-08-17 02:07:03', 39.99, 0);

--
-- Triggers `orders`
--
DELIMITER $$
CREATE TRIGGER `order_orderdetail_delete` AFTER DELETE ON `orders` FOR EACH ROW DELETE FROM orderdetails WHERE orders.id = orderdetails.id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(320) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `isadmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `address`, `postcode`, `password`, `isadmin`) VALUES
(1, 'Torry', 'torry@torry.com', '5146611768', '208#755 rue de la noue', 'H3E 1V1', 'Torry123', 1),
(2, 'Lenny', 'lenny@lenny.com', '5146611768', '208#755 rue de la noue', 'H3E 1V1', 'Lenny123', 0),
(3, 'Lorry', 'lorry@lorry.com', '5146611768', '208#755 rue de la noue', 'H3E 1V1', 'Lorry123', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bouquets`
--
ALTER TABLE `bouquets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `productID` (`productID`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bouquetid` (`bouquetid`),
  ADD KEY `orderNumber` (`orderNumber`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orderNumber` (`orderNumber`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bouquets`
--
ALTER TABLE `bouquets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `bouquets` (`id`);

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`bouquetid`) REFERENCES `bouquets` (`id`),
  ADD CONSTRAINT `orderdetails_ibfk_3` FOREIGN KEY (`orderNumber`) REFERENCES `orders` (`orderNumber`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
