<?php


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


require_once '_setup.php';

// STATE 1: first display
$app->get('/register', function ($request, $response, $args) {
    return $this->view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function ($request, $response, $args) {
    $registerInfo = $request->getParsedBody();
    $name = $registerInfo['name'];
    $email = $registerInfo['email'];
    $phone = $registerInfo['phone'];
    $address = $registerInfo['address']; 
    $postcode = $registerInfo['postcode'];  
    $password = $registerInfo['password'];
    $confirmPassword = $registerInfo['confirmPassword'];
    $isadmin =   0;                 
    
    $errorList = array();
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $name) != 1) { // no match
        array_push($errorList, "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.");
        $name = "";
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
        $email = "";
    } else {
        // is email already in use?
        $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }
    if (strlen($phone) < 6 || strlen($phone) > 24) {
        array_push($errorList, "phone must be 6~24 chars");
            $phone = "";
       
    } 

    if (strlen($postcode) < 3 || strlen($postcode) > 10) {
        array_push($errorList, "postcode must be 3~10 chars");
            $postcode = "";
        
    } 

    if (strlen($address) < 5 || strlen($address) > 60) {
        array_push($errorList, "address must be 5~60 chars");
        $address = "";
        
    } 

   

    if ($password != $confirmPassword) {
        array_push($errorList, "Passwords do not match");
    } else {
        if ((strlen($password) < 6) || (strlen($password) > 100)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
            array_push($errorList, "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }
    //
    if ($errorList) {
        return $this->view->render($response, 'register.html.twig',
                [ 'errorList' => $errorList, 'v' => [
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                    'phone' => $phone,
                    'postcode' => $postcode, 
                    'address' => $address  ,
                     ]  ]);
    } else {
        DB::insert('users', ['name' => $name, 
                           'email' => $email, 
                           'phone' => $phone,
                           'address' => $address ,
                           'postcode' => $postcode, 
                           'password' => $password,
                           'isAdmin' => $isadmin
         ]);
        
      
        return $this->view->render($response, 'register_success.html.twig');
    }


});

// used via AJAX
$app->get('/isemailtaken/[{email}]', function ($request, $response, $args) {
    $email = isset($args['email']) ? $args['email'] : "";
    $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($record) {
        return $response->write("Email already in use");
    } else {
        return $response->write("");
    }
});

// STATE 1: first display
$app->get('/login', function ($request, $response, $args) {
    // if (isset($_SESSION['user'])) {
    //     //TODO: Add loging message
    //     return $response->withHeader('Location', '/');
    // }

    return $this->view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function ($request, $response, $args) use ($log) {
    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['email']) && isset($loginInfo['password'])) {
            $user = DB::queryFirstRow("SELECT * FROM users WHERE email= %s", $loginInfo['email']);
            if ($loginInfo['password'] === $user['password']) {
                unset($user['password']);
                $_SESSION['user'] = $user;
                return $response
                    ->withHeader('Location', '/');
            }
        }
    }

    return $this->view->render($response, 'login.html.twig', [
        'error' => "Email doesn't match password."
    ]);
});
    
//     $email = $request->getParam('email');
//     $password = $request->getParam('password');
//     //
//     $record = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
//     $loginSuccess = false;
//     if ($record) {
//         if ($record['password'] == $password) {
//             $loginSuccess = true;
//         }        
//     }
//     //
//     if (!$loginSuccess) {
//         $log->info(sprintf("Login failed for email %s from %s", $email, $_SERVER['REMOTE_ADDR']));
//         return $this->view->render($response, 'login.html.twig', [ 'error' => true ]);
//     } else {
//         unset($record['password']); // for security reasons remove password from session
//         $_SESSION['user'] = $record; // remember user logged in
//         $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['id'], $_SERVER['REMOTE_ADDR']));
//         return $response ->withHeader('Location', '/') ;
//     }
// });


// STATE 1: first display
$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    unset($_SESSION['user']);   
    return $response->withHeader('Location', '/');
});



//      $app->get('/cart', function ($request, $response, $args) {
// 	// TODO: retrieve addProdId and quantity from URL *IF* present
//     $cartitemList = DB::query(
//                     "SELECT cartitems.ID as ID, productID, quantity,"
//                     . " name, imagefilepath, price ,description"
//                     . " FROM cartitems, bouquets "
//                     . " WHERE cartitems.productID = bouquets.id AND sessionID=%s", session_id());
//                     return $this->view->render($response, 'cart.html.twig', ['cartitemList' => $cartitemList ]);
   
// });




//choosemenu

$app->get('/choosemenu', function ($request, $response, $args) {
    
    $cartItems = DB::query("SELECT C.id AS id, M.name AS menuName,M.price AS price,
                            C.quantity AS quantity 
                            FROM cartitems AS C
                            INNER JOIN bouquets AS M
                                ON C.productID = M.id
                            WHERE sessionId = %s", session_id());
    

    $totalMoney =  DB::queryFirstField("SELECT SUM(M.price*C.quantity) AS totalMoney
                            FROM cartitems AS C
                            INNER JOIN bouquets AS M
                                ON C.productID = M.id
                            WHERE sessionId=%s",session_id());
    
    $menus = DB::query("SELECT * FROM bouquets" );
    $index = 1;
    foreach ($menus as &$menu) {
        $menu['index'] = $index++;
    }
    return $this->view->render(
        $response,
        'choosemenu.html.twig',
        [
            'menus' => $menus,
            'cartItems' => $cartItems,
            'totalMoney'=> $totalMoney
        ]
    );
});

// $app->post('/cart', function() use ($app) {
//     $productID = $app->request()->post('productID');
//     $quantity = $app->request()->post('quantity');
//     // FIXME: make sure the item is not in the cart yet
//     $item = DB::queryFirstRow("SELECT * FROM cartitems WHERE productID=%d AND sessionID=%s", $productID, session_id());
//     if ($item) {
//         DB::update('cartitems', array(
//             'sessionID' => session_id(),
//             'productID' => $productID,
//             'quantity' => $item['quantity'] + $quantity
//                 ), "productID=%d AND sessionID=%s", $productID, session_id());
//     } else {
//         DB::insert('cartitems', array(
//             'sessionID' => session_id(),
//             'productID' => $productID,
//             'quantity' => $quantity
//         ));
//     }
//     // show current contents of the cart
//     $cartitemList = DB::query(
//                     "SELECT cartitems.ID as ID, productID, quantity,"
//                     . " name, description, imagePath, price "
//                     . " FROM cartitems, bouquets "
//                     . " WHERE cartitems.productID = bouquets.ID AND sessionID=%s", session_id());
//     $app->render('cart.html.twig', array(
//         'cartitemList' => $cartitemList
//     ));
// });

$app->get('/api/cartitems', function ($request, $response, $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $paramArray = $request->getQueryParams();
    //$cartItems = DB::query("SELECT * FROM cartitems");
    $cartItems = DB::query("SELECT C.id AS id, M.name AS menuName,M.price AS price,
                            C.quantity AS quantity
                            FROM cartitems AS C
                            INNER JOIN bouquets AS M
                                ON C.productID = M.id
                            WHERE sessionId = %s", session_id());
    $json = json_encode($cartItems, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->get('/cart/add', function ($request, $response, $args) {
  
    $sessionId = session_id();
    $get = $request->getQueryParams();
    $message = 'faile';
    if (isset($get['productID'])) {
        $productID = $get['productID'];
        $quantity = $get['quantity'];
        $cartItem = DB::queryFirstRow(
            "SELECT quantity 
                    FROM cartitems 
                    WHERE sessionId = %s
                    AND productID = %i",
            $sessionId,
            $productID
           
        );
        if ($cartItem != null) {
            DB::update('cartitems', array(
                'sessionID' => $sessionId,
                'productID' => $productID,
                'quantity' => $cartItem['quantity'] + $quantity
            ), "productID=%d AND sessionID=%s", $productID, $sessionId);

            $message = "succeed";
        } else {
            DB::insert("cartitems", [
                'sessionId' => $sessionId,
                'productID' => $productID,
                'quantity' => $quantity
            ]);
            $message = "succeed";
        }
       
        $cartItems = DB::query("SELECT M.name AS menuName,M.price AS menuPrice,C.quantity AS quantity  
                                FROM cartitems AS C
                                INNER JOIN bouquets AS M
                                    ON C.productID = M.id
                                WHERE sessionId = %s", $sessionId);
        $menus = DB::query("SELECT *  FROM bouquets  ");
       
        $index = 1;
        foreach ($menus as &$menu) {
            $menu['index'] = $index++;
        }
        return $this->view->render(
            $response,
            'choosemenu.html.twig',
            ['cartItems' => $cartItems, 'menus' => $menus]
        );
    } else {
        $response->getBody()->write($message);
        return $response;
    }
});

$app->get('/cart/delete', function ($request, $response, $args) {
    $sessionId = session_id();
    $get = $request->getQueryParams();
    $message = 'failed';
    if (isset($get['productID'])) {
        $productID = $get['productID'];
        DB::delete('cartitems', 'productID=%i AND sessionId=%s', $productID, $sessionId);
        $message = "succeed";
        
        $cartItems = DB::query("SELECT M.name AS menuName,M.price AS menuPrice,C.quantity AS quantity  
                                FROM cartitems AS C
                                INNER JOIN bouquets AS M
                                    ON C.productID = M.id
                                WHERE sessionId = %s",  $sessionId);
        $menus = DB::query("SELECT * FROM bouquets  ");
       
        $index = 1;
        foreach ($menus as &$menu) {
            $menu['index'] = $index++;
        }
        return $this->view->render(
            $response,
            'chooseMenu.html.twig',
            [ 'cartItems' => $cartItems, 'menus' => $menus]
        );
    } else {
        $response->getBody()->write($message);
        return $response;
    }
});

$app->get('/placeorder/{totalMoney}', function ($request, $response, $args) {
    
    $totalMoney = isset($args['totalMoney']) ? $args['totalMoney'] : "";
    $cartitems = DB::query("SELECT * FROM cartitems WHERE sessionId = %s ",  session_id());
    $maxId = DB::queryFirstField("SELECT max(id) as maxId FROM orders");
    if ($maxId == NULL) {
        $maxId = 0;
    }
    $date = date('YmdHi');
    $orderNumber = (string) ($date) . (string) ($maxId + 1);
    DB::insert('orders', [
        'userid' => $_SESSION['user']['id'],
        'orderNumber' => $orderNumber,
        'totalprice' => $totalMoney
    ]);

    foreach ($cartitems as &$cartitem) {
        DB::insert('orderdetails', [
            'bouquetid' => $cartitem['productID'],
            'orderNumber' => $orderNumber,
            'quantity' => $cartitem['quantity'],
        ]);
        DB::delete(
            'cartitems',
            'productID=%i AND sessionId=%s ',
            $cartitem['productID'],
            session_id()
          
        );
    }
    return $this->view->render($response, 'placeorder.html.twig', ['orderNumber' => $orderNumber]);
});

$app->post('/orderview', function (Request $request, Response $response, array $args) {
   
    $checkoutInfo = $request->getParsedBody();
    $cartItems = DB::query("SELECT M.name AS menuName,M.price AS price,M.imagefilepath AS pictureFilePath,
                            C.quantity AS quantity  FROM cartitems AS C
                            INNER JOIN bouquets AS M
                            ON C.productid = M.id
                            WHERE sessionId = %s",  session_id());
    $subTotal = DB::queryFirstField("SELECT sum(M.price) AS subTotal FROM cartitems AS C
                            INNER JOIN bouquets AS M
                            ON C.productid = M.id
                            WHERE 
                            sessionId = %s", session_id());
    
    if (isset($_SESSION['user'])) {
        $users = DB::query("SELECT * FROM users WHERE id = %i",  $_SESSION['user']['id']);
    }
    return $this->view->render(
        $response,
        'orderview.html.twig',
        [
            'cartItems' => $cartItems,
            'subTotal' => $subTotal,
            'checkoutInfo' => $checkoutInfo,
           
            'users' => $users['0']
        ]
    );
});

$app->get('/checkout', function (Request $request, Response $response, array $args) {
   
    $cartItems = DB::query("SELECT M.name AS menuName,M.price AS price,M.imagefilepath AS pictureFilePath,
                            C.quantity AS quantity FROM cartitems AS C
                            INNER JOIN bouquets AS M
                            ON C.productid = M.id
                            WHERE sessionId = %s", session_id());
    $subTotal = DB::queryFirstField("SELECT sum(M.price) AS subTotal FROM cartitems AS C
                            INNER JOIN bouquets AS M
                            ON C.productid = M.id
                            WHERE  sessionId = %s", session_id());
   
    if (isset($_SESSION['user'])) {
        $users = DB::query("SELECT * FROM users WHERE id = %i",  $_SESSION['user']['id']);
    }
    return $this->view->render(
        $response,
        'checkout.html.twig',
        [
            'cartItems' => $cartItems,
            'subTotal' => $subTotal,
            'users' => $users['0']
        ]
    );
});

//user information========================
$app->get('/user/', function (Request $request, Response $response) {

    if( isset($_SESSION['user']) ){
        $user = DB::queryFirstRow("SELECT * FROM users 
        WHERE id= %s",$_SESSION['user']['id']);        
        return $this->view->render($response, 'userindex.html.twig',['user' => $user]);
    }else{
        return $response
        ->withHeader('Location', '/login');
    }    
});


$app->get('/userorderhistory', function (Request $request, Response $response) {
    
    $orders = DB::query("SELECT O.*, U.name AS customerName
                        FROM orders AS O
                        INNER JOIN users AS U
                            ON O.userid = U.id
                        WHERE O.userid=%i ", $_SESSION['user']['id']);
    //var_dump($orders);
    return $this->view->render(
        $response,
        'userorderhistory.html.twig',
        ['orders' => $orders]
    );
});

$app->PATCH('/api/users/phone', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
     global $log;
    $id = $_SESSION['user']['id'];
    $json = $request->getBody();
    $users = json_decode($json, true); // true makes it return an associative array instead of an object
    DB::query("update users set phone=%s where id=%d", $users['phone'], $id );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;
});

$app->PATCH('/api/users/address', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    //echo "test";
     global $log;
    $id = $_SESSION['user']['id'];
    $json = $request->getBody();
    $users = json_decode($json, true); 
    DB::query("update users set address=%s,postcode=%s where id=%s", 
            $users['address'], $users['postcode'], $id );
    $response->getBody()->write(json_encode(true)); 
    return $response;
});

$app->get('/userchangepassword', function (Request $request, Response $response) {
  
    return $this->view->render($response, 'userchangepassword.html.twig');
});

$app->post('/userchangepassword', function (Request $request, Response $response) {
   
    $registerInfo = $request->getParsedBody();

    $password = $registerInfo['password'];

    $confirmPassword = $registerInfo['confirmPassword'];
    $passQuality = verifyPassword($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }
    if (empty($errors)) {
        DB::query("update users set password=%s where id=%d",$password, $_SESSION['user']['id'] );
        return $this->view->render($response, 'userchangepassword.html.twig', ['changeSuccess' => true] );
    }

    return $this->view->render($response, 'userchangepassword.html.twig', [
        'errors' => $errors,
    ]);
});
function verifyPassword($password) {
    if (strlen($password) < 6 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false) {
        return "Password must be 6~100 characters,
                            must contain at least one uppercase letter, 
                            one lower case letter, 
                            and one number or special character.";
        }
    return TRUE;
}